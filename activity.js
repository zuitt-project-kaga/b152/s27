let http = require("http");

let items = [
  {
    name: "Iphone-x",
    price: 30000,
    isActive: true,
  },
  {
    name: "Samsung Galaxy S21",
    price: 51000,
    isActive: true,
  },
  {
    name: "Razer-Blackshark-VX2",
    price: 2800,
    isActive: false,
  },
];

http
  .createServer((req, res) => {
    let url = req.url;
    let method = req.method;

    console.log(url);
    console.log(method);

    if (url === "/items" && method === "GET") {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify(items));
    } else if (url === "/items" && method === "POST") {
      let addData = "";
      req.on("data", (byteData) => {
        addData += byteData;
      });
      req.on("end", () => {
        addData = JSON.parse(addData);
        let newItem = {
          name: addData.name,
          price: addData.price,
          isActive: addData.isActive
        };
        items.push(newItem);
        res.writeHead(200,{"Content-Type":"application/json"});
        res.end(JSON.stringify(items));
      });
    } else if (url === "/items" && method === "DELETE"){
        items.pop(items);
        res.writeHead(200,{"Content-Type":"application/json"});
        res.end(JSON.stringify(items));
    }
  })
  .listen(8000);

console.log("port 8000 read");
